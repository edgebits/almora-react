import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { userActions } from "../_actions";

class HomePage extends React.Component {
  componentDidMount() {}

  render() {
    const { user } = this.props;
    return (
      <div className="col-md-6 col-md-offset-3">
        {/* should be name! */}
        <h1>Hi {user.message}!</h1>
        <p>
          <Link to="/login">Log Out</Link>
        </p>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };
