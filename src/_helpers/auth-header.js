export function authHeader() {
    // return X-Access-Token
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return { 'X-Access-Token': user.token };
    } else {
        return {};
    }
}